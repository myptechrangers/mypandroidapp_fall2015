package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

public class EditChildrenInfo extends Activity {

    List<ParseObject> ob,obParent;
    ProgressDialog mProgressDialog;

    Bitmap bmp;


    EditText firstName,lastName,stuID,foodAllergy,gender,grade,leadRole,otherSchool,pantsWaist,shirtSize,shoeSize,tshirtSize,parentName,address,city,state,zip,extraShirtOne,extraShirtTwo,extraShirtThree,extraShortFour,fee,paymentMethod,schoolName,emailOne,emailTwo;
    ImageView childPicture;
    TextView phone;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_children_info);
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Child Details </font>"));
        firstName=(EditText) findViewById(R.id.firstname);
        lastName=(EditText) findViewById(R.id.lastname);
        stuID=(EditText) findViewById(R.id.studentID);
        foodAllergy=(EditText) findViewById(R.id.foodAllergy);
        gender=(EditText) findViewById(R.id.gender);
        grade=(EditText) findViewById(R.id.grade);
        leadRole=(EditText) findViewById(R.id.leadRole);
        otherSchool=(EditText) findViewById(R.id.otherSchool);
        pantsWaist=(EditText) findViewById(R.id.pantsWaist);
        shirtSize=(EditText) findViewById(R.id.shirtSize);
        shoeSize=(EditText) findViewById(R.id.shoeSize);
        tshirtSize=(EditText) findViewById(R.id.tshirtSize);
        parentName=(EditText) findViewById(R.id.parentName);
        address=(EditText) findViewById(R.id.address);
        city=(EditText) findViewById(R.id.city);
        state=(EditText) findViewById(R.id.state);
        zip=(EditText) findViewById(R.id.zip);
        extraShirtOne=(EditText) findViewById(R.id.extraShirtOne);
        extraShirtTwo=(EditText) findViewById(R.id.extraShirtTwo);
        extraShirtThree=(EditText) findViewById(R.id.extraShirtThree);
        extraShortFour=(EditText) findViewById(R.id.extraShirtFour);
        fee=(EditText) findViewById(R.id.fee);
        paymentMethod=(EditText) findViewById(R.id.paymentMethod);
        schoolName=(EditText) findViewById(R.id.schoolName);
        phone=(EditText) findViewById(R.id.phone);
        emailOne=(EditText) findViewById(R.id.emailOne);
        emailTwo=(EditText) findViewById(R.id.emailTwo);

        childPicture=(ImageView) findViewById(R.id.childPicture);

        i=ChildrenDetails.childDetails;
        byte[] byteArray = i.getByteArrayExtra("picture");
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        firstName.setText(i.getStringExtra("FirstName"));
        lastName.setText(i.getStringExtra("LastName"));
        stuID.setText(i.getStringExtra("StudentId"));
        foodAllergy.setText(i.getStringExtra("FoodAllergy"));
        gender.setText(i.getStringExtra("Gender"));
        grade.setText(i.getStringExtra("Grade"));
        leadRole.setText(i.getStringExtra("LeadRole"));
        pantsWaist.setText(i.getStringExtra("PantsWaist"));
        shirtSize.setText(i.getStringExtra("shirtSize"));
        shoeSize.setText(i.getStringExtra("shoeSize"));
        tshirtSize.setText(i.getStringExtra("TshortSize"));
        parentName.setText(i.getStringExtra("ParentName"));
        address.setText(i.getStringExtra("Address"));
        city.setText(i.getStringExtra("city"));
        state.setText(i.getStringExtra("state"));
        zip.setText(i.getStringExtra("zip"));
        extraShirtOne.setText(i.getStringExtra("extraShortOne"));
        extraShirtTwo.setText(i.getStringExtra("extraShirtTwo"));
        extraShirtThree.setText(i.getStringExtra("extraShirtThree"));
        extraShortFour.setText(i.getStringExtra("extraShirtFour"));
        fee.setText(i.getStringExtra("Fee"));
        paymentMethod.setText(i.getStringExtra("PaymentMethod"));
        schoolName.setText(i.getStringExtra("schoolName"));
        phone.setText(i.getStringExtra("phone"));
        emailTwo.setText(i.getStringExtra("EmailTwo"));
        emailOne.setText(i.getStringExtra("EmailOne"));
        //childPicture.setImageBitmap(bmp);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_children_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Save:
                ParseQuery<ParseObject> query=ParseQuery.getQuery("Children");
                ParseQuery<ParseObject> queryParent=ParseQuery.getQuery("Parent");
                String phno=phone.getText().toString();
                ParseObject child = null;
                query.whereEqualTo("PhoneNumber",phno);

                try {
                    child = query.getFirst();

                    if(child != null)
                    {
                        child.put("FirstName", firstName.getText().toString());
                        child.put("LastName", lastName.getText().toString());
                        child.put("FoodAllergy",foodAllergy.getText().toString());
                        child.put("Grade",grade.getText().toString());
                        child.put("Gender",gender.getText().toString());
                        child.put("LeadRole",leadRole.getText().toString());
                        child.put("OtherSchool",otherSchool.getText().toString());
                        child.put("PantsWaist",pantsWaist.getText().toString());
                        child.put("School",schoolName.getText().toString());
                        child.put("ShirtSize",shirtSize.getText().toString());
                        child.put("ShoeSize",shoeSize.getText().toString());
                        child.put("TshirtSize",tshirtSize.getText().toString());

                        child.save();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                ParseObject parent = null;
                queryParent.whereEqualTo("PhoneNumber",phno);

                try {
                    parent = queryParent.getFirst();

                    if(parent != null) {
                        parent.put("Name",parentName.getText().toString());
                        parent.put("Address",address.getText().toString());
                        parent.put("City",city.getText().toString());
                        parent.put("EmailOne",emailOne.getText().toString());
                        parent.put("EmailTwo",emailTwo.getText().toString());
                        parent.put("ExtraShirtOne",extraShirtOne.getText().toString());
                        parent.put("ExtraShirtTwo",extraShirtTwo.getText().toString());
                        parent.put("ExtraShirtThree",extraShirtThree.getText().toString());
                        // parent.put("ExtraShirtFour",extraShirtFour.getText().toString());
                        parent.put("PaymentMethod",paymentMethod.getText().toString());
                        parent.put("Fee",fee.getText().toString());
                        parent.put("State",state.getText().toString());
                        parent.put("Zip",zip.getText().toString());

                        parent.save();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Intent i=new Intent(EditChildrenInfo.this,ChildInfo_Updated.class);
                startActivity(i);
                break;



            case R.id.item_home:
                Intent intent7 = new Intent(this,parentHomeScreen.class);
                startActivity(intent7);
                break;
            case R.id.item_events:
                Intent intent = new Intent(this,CalanderParent_Updated.class);
                startActivity(intent);
                break;
            case R.id.item_childinfo:
                Intent intent1 = new Intent(this,ChildInfo_Updated.class);
                startActivity(intent1);
                break;
            case R.id.item_contactus:
                Intent intent2 = new Intent(this,contactUs.class);
                startActivity(intent2);
                break;
            case R.id.item_notifications:
                Intent intent3 = new Intent(this,Notifications.class);
                startActivity(intent3);
                break;
            case R.id.item_donateus:
                Intent intent4 = new Intent(this,ParentDonation.class);
                startActivity(intent4);
                break;
            case R.id.item_socialmedia:
                Intent intent5 = new Intent(this,SocialMedia.class);
                startActivity(intent5);
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
