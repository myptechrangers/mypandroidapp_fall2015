package com.example.s521691.maryvilleyoungplayers;


/**
 * Created by S521691 on 11/4/2015.
 */
public class ParentNewsFeeds  {
    private String title;
    private String desc;
    private String date;
    private String type;
    private String img;

    public ParentNewsFeeds(String title,String desc, String date, String type,String img) {
        this.title = title;
        this.desc = desc;
        this.date = date;
        this.type = type;
        this.img=img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
