package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class SocialMedia extends Activity {

    ImageButton fb,flickr,youtube;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media);
        addListenerOnImageButton();

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Socialmedia </font>"));
    }

    public void addListenerOnImageButton(){
        fb = (ImageButton)findViewById(R.id.fb_SM);
        flickr = (ImageButton)findViewById(R.id.flickr_SM);
        youtube = (ImageButton)findViewById(R.id.youtube_SM);

        fb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent fb = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/MaryvilleYoungPlayers?fref=ts"));
                startActivity(fb);

            }
        });
        flickr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent flickr = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.flickr.com/"));
                startActivity(flickr);

            }
        });
        youtube.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent youtube = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/"));
                startActivity(youtube);

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_social_media, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_home:
                Intent intent7 = new Intent(this,parentHomeScreen.class);
                startActivity(intent7);
                break;
            case R.id.item_events:
                Intent intent = new Intent(this,CalanderParent_Updated.class);
                startActivity(intent);
                break;
            case R.id.item_childinfo:
                Intent intent1 = new Intent(this,ChildInfo_Updated.class);
                startActivity(intent1);
                break;
            case R.id.item_contactus:
                Intent intent2 = new Intent(this,contactUs.class);
                startActivity(intent2);
                break;
            case R.id.item_notifications:
                Intent intent3 = new Intent(this,Notifications.class);
                startActivity(intent3);
                break;
            case R.id.item_donateus:
                Intent intent4 = new Intent(this,ParentDonation.class);
                startActivity(intent4);
                break;
            case R.id.item_socialmedia:
                Intent intent5 = new Intent(this,SocialMedia.class);
                startActivity(intent5);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
