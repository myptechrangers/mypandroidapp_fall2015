package com.example.s521691.maryvilleyoungplayers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ChildInfo_adapter extends BaseAdapter {

    ArrayList<ChildrenData> list;
    Context mContext;

    public ChildInfo_adapter(Context con, ArrayList<ChildrenData> list) {
        this.mContext = con;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.childlist_rowlayout, null);

            holder = new Holder();
            holder.firstName = (TextView) convertView.findViewById(R.id.childlist_Fname);
            holder.lastName = (TextView) convertView.findViewById(R.id.childlist_Lname);
            holder.cast = (TextView) convertView.findViewById(R.id.cast);
            holder.delimiter=(TextView) convertView.findViewById(R.id.delimiter);
            holder.image=(ImageView) convertView.findViewById(R.id.childlist_img);
            holder.role=(TextView) convertView.findViewById(R.id.Role);

            convertView.setTag(holder);

        }
        else {

            holder = (Holder) convertView.getTag();
        }

        holder.firstName.setText(list.get(position).getFirstName());
        holder.lastName.setText(list.get(position).getLastName());
        if(list.get(position).getCast().equals("1")) {
            holder.cast.setText("White");
        }
        else if(list.get(position).getCast().equals("2")) {
            holder.cast.setText("Green");
        }
        else if(list.get(position).getCast().equals("3")) {
            holder.cast.setText("Staff");
        }
        else if(list.get(position).getCast().equals("4")) {
            holder.cast.setText("Others");
        }
        holder.delimiter.setText("  ");
        holder.image.setImageBitmap(list.get(position).getChildImage());
        holder.role.setText(list.get(position).getRole());


        return convertView;
    }

    private class Holder {
        TextView firstName ;
        TextView lastName;
        TextView cast;
        TextView delimiter;
        ImageView image;
        TextView role;
    }
}
