package com.example.s521691.maryvilleyoungplayers;


import android.graphics.Bitmap;

public class ChildrenData {
    private String firstName, lastName,cast,space,role;
    private Bitmap ChildImage;

    public ChildrenData(String fname, String lname,String cast,String role,String space, Bitmap ChildImage) {
        this.firstName = fname;
        this.cast=cast;
        this.space=" ";
        this.lastName = lname;
        this.ChildImage=ChildImage;
        this.role=role;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getCast() {
        return cast;
    }

    public String getSpace() {
        return space;
    }
    public void setCast(String cast) {
        this.cast = cast;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Bitmap getChildImage() {
        return ChildImage;
    }

    public void setChildImage(Bitmap childImage) {
        ChildImage = childImage;
    }
}
