package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import twitter4j.conf.*;

//import org.apache.http.HttpResponse;
//import org.apache.http.HttpStatus;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.util.EntityUtils;


public class parentHomeScreen extends Activity implements Comparator<ParentNewsFeeds> {

    ListView listview;
    List<ParseObject> ob,obEvents,obAll,obMsgType;
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> adapter;
    ArrayList<ParentNewsFeeds> list=new ArrayList<>();
    Boolean flag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_home_screen);
        ParseQuery<ParseObject> queryNotification = new ParseQuery<ParseObject>("Notifications");
        queryNotification.whereEqualTo("PhoneNumber",MainActivity.PH_NO);
        try{
            obMsgType=queryNotification.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for(ParseObject obj:obMsgType){
            if(obj.get("MessageType").equals("P") && obj.get("IsRead").equals("U")){
               flag=true;
            }
        }
        if(flag==true) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(parentHomeScreen.this);
            alertDialogBuilder.setTitle("Priority Message!!! ");
            alertDialogBuilder

                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        new RemoteDataTask().execute();

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Home </font>"));
    }



    @Override
    public int compare(ParentNewsFeeds lhs, ParentNewsFeeds rhs) {
        return lhs.getDate().compareTo(rhs.getDate());
    }


    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(parentHomeScreen.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Data from parse");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Notifications");
            query.whereEqualTo("PhoneNumber",MainActivity.PH_NO);
            query.orderByAscending("_created_at");
            ParseQuery<ParseObject> queryEvents = new ParseQuery<ParseObject>("Events");
            queryEvents.orderByAscending("_created_at");
            try {
                ob = query.find();
                obEvents=queryEvents.find();
               for(int i=0;i<ob.size();i++){
                   obAll.add(ob.get(i));
               }
                for(int i=0;i<obEvents.size();i++){
                    obAll.add(obEvents.get(i));
                }
//                ConfigurationBuilder cb=new ConfigurationBuilder();
//                cb.setOAuthConsumerKey("RCHsSoVYINgaFJ0T9klbYqmUv\n");
//                cb.setOAuthConsumerSecret("kBjrMdji5zfOHRE3z9tWpEvO8vBQTAwRL4izmtAcizQLD9dgKj");
//                cb.setOAuthAccessToken("3828820872-ciHRYtDidPHcDwfzdTQY2foJ3nXq7ki1gvBgfgC");
//                cb.setOAuthAccessTokenSecret("rSCPUvM9Sk7G8Bk1hwiW6YuRjDsrZ4mftVeudHcmNt4QH");
//                TwitterFactory tf=new TwitterFactory(cb.build());
//                Twitter twitter=tf.getInstance();
//                Paging paging=new Paging(1,5);
//                List<Status> statuses=twitter.getUserTimeline();
//              //  statuses=twitter.getUserTimeline();
////                list.clear();
//                for (    Status s : statuses) {
//                    list.add(new ParentNewsFeeds("",s.toString()," "," "," "));
//                }
//                //mTweets.notifyDataSetChanged();
//            }
//            catch (  TwitterException e) {
//                e.printStackTrace();
//            }


             }

             catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            catch(Exception ex){
                    Log.e("TwitterFeedActivity", "Error loading JSON", ex);
                }
                return null;
            }

        @Override
        protected void onPostExecute(Void result) {
            listview = (ListView) findViewById(R.id.newsFeeds);
            adapter = new ArrayAdapter<String>(parentHomeScreen.this,R.layout.parentnewsfeeds_layout);
            for (ParseObject messages : ob) {
                adapter.add((String) messages.get("Message"));
                Date date=messages.getCreatedAt();
                Format formatter = new SimpleDateFormat("MMM dd, yyyy HH:mm a");
                String s = formatter.format(date);
                list.add(new ParentNewsFeeds( "Message",(messages.get("Message")).toString(),s,"N",""));
            }
            for(ParseObject events:obEvents){
                Date date=events.getDate("EventDate");
                Format formatter = new SimpleDateFormat("MMM dd, yyyy HH:mm a");
                String s = formatter.format(date);
                list.add(new ParentNewsFeeds((String)events.get("Title"),events.get("Description").toString(),s,"E",""));
            }
            Collections.sort(list, new Comparator<ParentNewsFeeds>() {
                @Override
                public int compare(ParentNewsFeeds lhs, ParentNewsFeeds rhs) {
                    return rhs.getDate().compareTo(lhs.getDate());
                }
            });
         listview.setAdapter(new ParentNewsFeeds_adapter(parentHomeScreen.this, list));

            mProgressDialog.dismiss();
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent i = new Intent(parentHomeScreen.this, ParentHomeScreenDetails.class);
                    i.putExtra("Title", list.get(position).getTitle());
                    i.putExtra("Desc",list.get(position).getDesc());
                    i.putExtra("date",list.get(position).getDate());

                    startActivity(i);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_parent_home_screen,menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_home:
                Intent intent7 = new Intent(this,parentHomeScreen.class);
                startActivity(intent7);
                break;
            case R.id.item_events:
                Intent intent = new Intent(this,CalanderParent_Updated.class);
                startActivity(intent);
                break;
            case R.id.item_childinfo:
                Intent intent1 = new Intent(this,ChildInfo_Updated.class);
                startActivity(intent1);
                break;
            case R.id.item_contactus:
                Intent intent2 = new Intent(this,contactUs.class);
                startActivity(intent2);
                break;
            case R.id.item_notifications:
                Intent intent3 = new Intent(this,Notifications.class);
                startActivity(intent3);
                break;
            case R.id.item_donateus:
                Intent intent4 = new Intent(this,ParentDonation.class);
                startActivity(intent4);
                break;
            case R.id.item_socialmedia:
                Intent intent5 = new Intent(this,SocialMedia.class);
                startActivity(intent5);
                break;
            case R.id.item_logout:
                Intent intent6 = new Intent(this,MainActivity.class);
                startActivity(intent6);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
