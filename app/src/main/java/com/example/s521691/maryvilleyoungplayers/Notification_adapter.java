package com.example.s521691.maryvilleyoungplayers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by S521691 on 11/4/2015.
 */
public class Notification_adapter extends BaseAdapter {
    ArrayList<NotificationClass> list;
    Context mContext;

    public Notification_adapter(Context con, ArrayList<NotificationClass> list) {
        this.mContext = con;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.notifications_rowlayout, null);

            holder = new Holder();
            holder.message = (TextView) convertView.findViewById(R.id.notification_message);
            holder.date = (TextView) convertView.findViewById(R.id.notification_date);
            holder.image=(ImageView) convertView.findViewById(R.id.notifiactions_img);

            holder.type=(TextView) convertView.findViewById(R.id.notification_type);

            convertView.setTag(holder);

        }
        else {

            holder = (Holder) convertView.getTag();
        }

        holder.message.setText(list.get(position).getMessage());
        holder.date.setText(list.get(position).getDate());
        if(list.get(position).getReadUnread().equals("R")) {
            holder.image.setImageResource(R.drawable.readmsg);
        }
        else if(list.get(position).getReadUnread().equals("U")) {
            holder.image.setImageResource(R.drawable.unreadmsg);
        }

        if(list.get(position).getType().equals("P")) {
            holder.type.setText("Priority");
        }
        else if(list.get(position).getType().equals("N")){
            holder.type.setText("Normal");
        }
        return convertView;
    }

    private class Holder {
        TextView message ;
        TextView date;
        ImageView image;
        TextView type;
    }
}
