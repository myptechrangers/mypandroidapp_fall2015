package com.example.s521691.maryvilleyoungplayers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class CheckableListForSpecificRecipents extends Activity {

    ListView listview;
    List<ParseObject> ob;
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> adapter;
    ArrayList<String> phoneNumbers=new ArrayList<>();
    ArrayList<String> emails=new ArrayList<>();
    static String appendedEmails="",appendedPhoneNumbers="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkable_list_for_specific_recipents);
        new RemoteDataTask().execute();
    }

    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(CheckableListForSpecificRecipents.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Loading Children details");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Locate the class table named "Country" in Parse.com
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Children");
            query.orderByDescending("_created_at");
            try {
                ob = query.find();
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.recipentsListView);
            // Pass the results into an ArrayAdapter
            adapter = new ArrayAdapter<String>(CheckableListForSpecificRecipents.this,R.layout.specific_recipents_rowlayout);
            // Retrieve object "name" from Parse.com database
            for (ParseObject children : ob) {
                adapter.add((String) children.get("FirstName"));
                phoneNumbers.add(children.get("PhoneNumber").toString());
                // emails.add(children.get("Email").toString());
            }
            for (int i=0;i<emails.size();i++){
                appendedEmails+= emails.get(i)+ ",";
            }
            for(int i=0;i<phoneNumbers.size();i++){
                appendedPhoneNumbers+=phoneNumbers.get(i)+",";
            }

            // Binds the Adapter to the ListView
            listview.setAdapter(adapter);

        }
    }

}
