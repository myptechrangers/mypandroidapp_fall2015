package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class MessagingCentre extends Activity {

    ListView listview;
    List<ParseObject> ob,obParent;
    ArrayAdapter<String> adapter;
    Button send,cancel,addSpecific;
    RadioGroup radioGroupAllSpecific,radioGroupNormalPriority;
    RadioButton normal, priority, all, specific;
    CheckBox mail, msg, notification;
    ArrayList<String> phoneNumbers=new ArrayList<>();
    ArrayList<String> emails=new ArrayList<>();
    String appendedEmails="",appendedPhoneNumbers="";
    EditText message,subject;
    String[] emailsWithDelimiter;
    Context context;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging_centre);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        new RemoteDataTask().execute();

        send = (Button) findViewById(R.id.sendBTNMsgCentre);
        normal = (RadioButton) findViewById(R.id.normalRadioButton);
        priority = (RadioButton) findViewById(R.id.priorityRadioButton);
        all = (RadioButton) findViewById(R.id.AllRadioButton);
        specific = (RadioButton) findViewById(R.id.specificRadioButton);
        mail = (CheckBox) findViewById(R.id.mailCheckBox);
        msg = (CheckBox) findViewById(R.id.msgCheckbox);
        notification = (CheckBox) findViewById(R.id.notificationCheckBox);
        message=(EditText) findViewById(R.id.MsgEditText);
        subject=(EditText) findViewById(R.id.subject);
        addSpecific=(Button)findViewById(R.id.addSpecific);
        addSpecific.setEnabled(false);

        addSpecific.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addSpecific.isSelected()){
                    addSpecific.setEnabled(true);
                }
            }
        });



        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (normal.isChecked()) {
                    if (all.isChecked()) {

                        if (mail.isChecked() && msg.isChecked() && notification.isChecked()) {

                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("message/rfc822");
                                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"MaryvilleYoungPlayers@gmail.com"});
                                i.putExtra(Intent.EXTRA_CC, new String[]{appendedEmails});
                                i.putExtra(Intent.EXTRA_SUBJECT,subject.getText().toString());
                                i.putExtra(Intent.EXTRA_TEXT, message.getText());
                                startActivity(Intent.createChooser(i, "Send mail..."));
                            Intent sms=new Intent(getApplicationContext(),SMS.class);
                            sms.putExtra("phone",appendedPhoneNumbers);
                            sms.putExtra("message", message.getText().toString());
                            startActivity(sms);
                            ParseObject notifications=new ParseObject("Notifications");
                            for(String phno1:phoneNumbers) {
                                notifications.put("Message", message.getText().toString());
                                notifications.put("Subject",subject.getText().toString());
                                notifications.put("PhoneNumber", phno1.toString());
                                notifications.put("MessageType","N");
                                notifications.put("IsRead","U");
                                notifications.saveInBackground();

                            }
                            Toast.makeText(getApplicationContext(),
                                    "Notification",
                                    Toast.LENGTH_LONG).show();



                        } else if (mail.isChecked() && msg.isChecked()) {


                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"MaryvilleYoungPlayers@gmail.com"});
                            i.putExtra(Intent.EXTRA_CC, new String[]{appendedEmails});
                            i.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
                            i.putExtra(Intent.EXTRA_TEXT, message.getText());
                            startActivity(Intent.createChooser(i, "Send mail..."));
                            Intent sms=new Intent(getApplicationContext(),SMS.class);
                            sms.putExtra("phone",appendedPhoneNumbers);
                            sms.putExtra("message", message.getText().toString());
                            startActivity(sms);





                        } else if (mail.isChecked() && notification.isChecked()) {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"MaryvilleYoungPlayers@gmail.com"});
                            i.putExtra(Intent.EXTRA_CC, new String[]{appendedEmails});
                            i.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
                            i.putExtra(Intent.EXTRA_TEXT, message.getText());
                            startActivity(Intent.createChooser(i, "Send mail..."));
                            ParseObject notifications=new ParseObject("Notifications");
                            for(String phno1:phoneNumbers) {
                                notifications.put("Message", message.getText().toString());
                                notifications.put("PhoneNumber", phno1.toString());
                                notifications.put("Subject",subject.getText().toString());
                                notifications.put("MessageType","N");
                                notifications.put("IsRead","U");
                                notifications.saveInBackground();

                            }
                            Toast.makeText(getApplicationContext(),
                                    "Notification",
                                    Toast.LENGTH_LONG).show();

                        } else if (msg.isChecked() && notification.isChecked()) {
                            Intent sms=new Intent(getApplicationContext(),SMS.class);
                            sms.putExtra("phone",appendedPhoneNumbers);
                            sms.putExtra("message", message.getText().toString());
                            startActivity(sms);
                            ParseObject notifications=new ParseObject("Notifications");
                            for(String phno1:phoneNumbers) {
                                notifications.put("Message", message.getText().toString());
                                notifications.put("PhoneNumber", phno1.toString());
                                notifications.put("Subject",subject.getText().toString());
                                notifications.put("MessageType","N");
                                notifications.put("IsRead","U");
                                notifications.saveInBackground();

                            }
                            Toast.makeText(getApplicationContext(),
                                    "Notification",
                                    Toast.LENGTH_LONG).show();

                        } else if (mail.isChecked()) {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"MaryvilleYoungPlayers@gmail.com"});
                            i.putExtra(Intent.EXTRA_CC, new String[]{appendedEmails});
                            i.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
                            i.putExtra(Intent.EXTRA_TEXT, message.getText());
                            startActivity(Intent.createChooser(i, "Send mail..."));

                        } else if (msg.isChecked()) {
                            Intent sms=new Intent(getApplicationContext(),SMS.class);
                            sms.putExtra("phone",appendedPhoneNumbers);
                            sms.putExtra("message", message.getText().toString());
                            startActivity(sms);

                        } else if (notification.isChecked()) {

                            for(String phno1:phoneNumbers) {
                                ParseObject notifications=new ParseObject("Notifications");
                                notifications.put("Message", message.getText().toString());
                                notifications.put("PhoneNumber", phno1.toString());
                                notifications.put("Subject",subject.getText().toString());
                                notifications.put("MessageType","N");
                                notifications.put("IsRead","U");
                                notifications.saveInBackground();

                            }
                            Toast.makeText(getApplicationContext(),
                                    "Notification",
                                    Toast.LENGTH_LONG).show();

                        } else {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder
                                    (MessagingCentre.this);

                            alertDialogBuilder.setTitle("Please select any of the medium to send message ");
                            alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }


                    } else if (specific.isChecked()) {
                       // addSpecific.setEnabled(true);
                        if(addSpecific.isEnabled()){
                            addSpecific.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent=new Intent(getApplicationContext(),CheckableListForSpecificRecipents.class);
                                    startActivity(intent);
                                }
                            });
                        }

                    }


                } else if (priority.isChecked())

                {
                    if (all.isChecked()) {
                        if (mail.isChecked() && msg.isChecked() && notification.isChecked()) {

                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"MaryvilleYoungPlayers@gmail.com"});
                            i.putExtra(Intent.EXTRA_CC, new String[]{appendedEmails});
                            i.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
                            i.putExtra(Intent.EXTRA_TEXT, message.getText());
                            startActivity(Intent.createChooser(i, "Send mail..."));
                            Intent sms = new Intent(getApplicationContext(), SMS.class);
                            sms.putExtra("phone", appendedPhoneNumbers);
                            sms.putExtra("message", message.getText().toString());
                            startActivity(sms);
                            ParseObject notifications = new ParseObject("Notifications");
                            for (String phno1 : phoneNumbers) {
                                notifications.put("Message", message.getText().toString());
                                notifications.put("PhoneNumber", phno1.toString());
                                notifications.put("Subject",subject.getText().toString());
                                notifications.put("MessageType", "P");
                                notifications.put("IsRead","U");
                                notifications.saveInBackground();

                            }
                            Toast.makeText(getApplicationContext(),
                                    "Notification",
                                    Toast.LENGTH_LONG).show();


                        } else if (mail.isChecked() && msg.isChecked()) {


                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"MaryvilleYoungPlayers@gmail.com"});
                            i.putExtra(Intent.EXTRA_CC, new String[]{appendedEmails});
                            i.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
                            i.putExtra(Intent.EXTRA_TEXT, message.getText());
                            startActivity(Intent.createChooser(i, "Send mail..."));
                            Intent sms = new Intent(getApplicationContext(), SMS.class);
                            sms.putExtra("phone", appendedPhoneNumbers);
                            sms.putExtra("message", message.getText().toString());
                            startActivity(sms);


                        } else if (mail.isChecked() && notification.isChecked()) {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"MaryvilleYoungPlayers@gmail.com"});
                            i.putExtra(Intent.EXTRA_CC, new String[]{appendedEmails});
                            i.putExtra(Intent.EXTRA_SUBJECT,subject.getText().toString());
                            i.putExtra(Intent.EXTRA_TEXT, message.getText());
                            startActivity(Intent.createChooser(i, "Send mail..."));
                            ParseObject notifications = new ParseObject("Notifications");
                            for (String phno1 : phoneNumbers) {
                                notifications.put("Message", message.getText().toString());
                                notifications.put("PhoneNumber", phno1.toString());
                                notifications.put("Subject",subject.getText().toString());
                                notifications.put("MessageType", "P");
                                notifications.put("IsRead","U");
                                notifications.saveInBackground();

                            }
                            Toast.makeText(getApplicationContext(),
                                    "Notification",
                                    Toast.LENGTH_LONG).show();

                        } else if (msg.isChecked() && notification.isChecked()) {
                            Intent sms = new Intent(getApplicationContext(), SMS.class);
                            sms.putExtra("phone", appendedPhoneNumbers);
                            sms.putExtra("message", message.getText().toString());
                            startActivity(sms);
                            ParseObject notifications = new ParseObject("Notifications");
                            for (String phno1 : phoneNumbers) {
                                notifications.put("Message", message.getText().toString());
                                notifications.put("PhoneNumber", phno1.toString());
                                notifications.put("Subject",subject.getText().toString());
                                notifications.put("MessageType", "P");
                                notifications.put("IsRead","U");
                                notifications.saveInBackground();

                            }
                            Toast.makeText(getApplicationContext(),
                                    "Notification",
                                    Toast.LENGTH_LONG).show();

                        } else if (mail.isChecked()) {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"MaryvilleYoungPlayers@gmail.com"});
                            i.putExtra(Intent.EXTRA_CC, new String[]{appendedEmails});
                            i.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
                            i.putExtra(Intent.EXTRA_TEXT, message.getText());
                            startActivity(Intent.createChooser(i, "Send mail..."));

                        } else if (msg.isChecked()) {
                            Intent sms = new Intent(getApplicationContext(), SMS.class);
                            sms.putExtra("phone", appendedPhoneNumbers);
                            sms.putExtra("message", message.getText().toString());
                            startActivity(sms);

                        } else if (notification.isChecked()) {

                            for (String phno1 : phoneNumbers) {
                                ParseObject notifications = new ParseObject("Notifications");
                                notifications.put("Message", message.getText().toString());
                                notifications.put("PhoneNumber", phno1.toString());
                                notifications.put("Subject",subject.getText().toString());
                                notifications.put("MessageType", "P");
                                notifications.put("IsRead","U");
                                notifications.saveInBackground();

                            }
                            Toast.makeText(getApplicationContext(),
                                    "Notification",
                                    Toast.LENGTH_LONG).show();

                        } else {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder
                                    (MessagingCentre.this);
                            alertDialogBuilder.setTitle("Please select any of the medium to send message ");
                            alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                    }else if (specific.isChecked()) {

                    }
                }
            }

        });

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Messaging Center </font>"));
    }
    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Children");
            query.orderByDescending("_created_at");
            try {
                ob = query.find();
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            listview = (ListView) findViewById(R.id.recipentsListView);
            adapter = new ArrayAdapter<String>(MessagingCentre.this,R.layout.messagingcentre_rowlayout);
            for (ParseObject children : ob) {
                adapter.add((String) children.get("FirstName"));
                phoneNumbers.add(children.get("PhoneNumber").toString());
                ParseQuery<ParseObject> queryParent = new ParseQuery<ParseObject>("Parent");
                queryParent.whereEqualTo("PhoneNumber", children.getString("PhoneNumber"));
                try{
                    obParent=queryParent.find();
                }
                catch (ParseException e){
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                for(ParseObject parent: obParent){
                    emails.add(parent.getString("EmailOne"));
                }

            }
            for (int i=0;i<emails.size();i++){
                appendedEmails+= emails.get(i)+ ",";
            }
            for(int i=0;i<phoneNumbers.size();i++){
                appendedPhoneNumbers+=phoneNumbers.get(i)+",";
            }
            // Binds the Adapter to the ListView
            listview.setAdapter(adapter);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_messaging_centre, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.item_adminhome:
                Intent intent3= new Intent(this, adminHomePage.class);
                startActivity(intent3);
                break;
            case R.id.item_studentList:
                Intent intent4= new Intent(this, StudentList_admin.class);
                startActivity(intent4);
                break;
            case R.id.item_adminevents:
                Intent intent = new Intent(this, AddEventPageAdmin.class);
                startActivity(intent);
                break;
            case R.id.item_messaging:
                Intent intent1 = new Intent(this, MessagingCentre.class);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
