package com.example.s521691.maryvilleyoungplayers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Calendar_adapter extends BaseAdapter {

    ArrayList<Events> list;
    Context mContext;

    public Calendar_adapter(Context con, ArrayList<Events> list) {
        this.mContext = con;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.calander_rowlayout, null);

            holder = new Holder();
            holder.cal_name = (TextView) convertView.findViewById(R.id.calendar_row_title);
            holder.cal_desc = (TextView) convertView.findViewById(R.id.calendar_row_desc);
            holder.image=(ImageView) convertView.findViewById(R.id.events_img);
            holder.date=(TextView) convertView.findViewById(R.id.calendar_row_date);


            convertView.setTag(holder);

        }
        else {

            holder = (Holder) convertView.getTag();
        }

        holder.cal_name.setText(list.get(position).getEventName());
        holder.cal_desc.setText(list.get(position).getEventDesc());
        holder.image.setImageResource(R.drawable.calendar);
        holder.date.setText(list.get(position).getEventDate());
        return convertView;
    }

    private class Holder {
        TextView cal_name ;
        TextView cal_desc;
        ImageView image;
        TextView date;
    }
}
