package com.example.s521691.maryvilleyoungplayers;

public class Events {
    private String eventName, eventDesc, eventDate;

    public Events(String name, String desc,String date) {
        this.eventName = name;
        this.eventDesc = desc;
        this.eventDate = date;
    }



    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

}
