package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class contactUs extends Activity {

    ImageButton call,call2,mail,mail2;
    Button emailUs,fB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Contact Us </font>"));

        call=(ImageButton) findViewById(R.id.callBTNContactUsVanessa);

        call.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:2019816654"));
                startActivity(callIntent);
            }
        });

        call2=(ImageButton) findViewById(R.id.callBTNContactUsPAT);

        call2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:2019816654"));
                startActivity(callIntent);
            }
        });
        mail=(ImageButton) findViewById(R.id.mailBTNContactUsVanessa);

        mail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
               // i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"parsons@k12.maryville.mo.us"});
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"ghanta1993@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                i.putExtra(Intent.EXTRA_TEXT, "body of email");
                startActivity(Intent.createChooser(i, "Send mail..."));

            }
        });
        mail2=(ImageButton) findViewById(R.id.mailBTNContactUsPAT);

        mail2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
               // i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"pimmel@nwmissouri.edu"});
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"ghanta1993@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                i.putExtra(Intent.EXTRA_TEXT   , "body of email");
                startActivity(Intent.createChooser(i, "Send mail..."));

            }
        });
        emailUs=(Button) findViewById(R.id.emailUsBTN);
        emailUs.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                // i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"pimmel@nwmissouri.edu"});
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"ghanta1993@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                i.putExtra(Intent.EXTRA_TEXT   , "body of email");
                startActivity(Intent.createChooser(i, "Send mail..."));
            }
        });
        fB=(Button) findViewById(R.id.fbBTN);
        fB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fb = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/MaryvilleYoungPlayers?fref=ts"));
                startActivity(fb);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_contact_us, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_home:
                Intent intent7 = new Intent(this,parentHomeScreen.class);
                startActivity(intent7);
                break;
            case R.id.item_events:
                Intent intent = new Intent(this,CalanderParent_Updated.class);
                startActivity(intent);
                break;
            case R.id.item_childinfo:
                Intent intent1 = new Intent(this,ChildInfo_Updated.class);
                startActivity(intent1);
                break;
            case R.id.item_contactus:
                Intent intent2 = new Intent(this,contactUs.class);
                startActivity(intent2);
                break;
            case R.id.item_notifications:
                Intent intent3 = new Intent(this,Notifications.class);
                startActivity(intent3);
                break;
            case R.id.item_donateus:
                Intent intent4 = new Intent(this,ParentDonation.class);
                startActivity(intent4);
                break;
            case R.id.item_socialmedia:
                Intent intent5 = new Intent(this,SocialMedia.class);
                startActivity(intent5);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
