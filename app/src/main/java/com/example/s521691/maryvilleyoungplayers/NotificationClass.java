package com.example.s521691.maryvilleyoungplayers;

/**
 * Created by S521691 on 11/4/2015.
 */
public class NotificationClass {
    private String objectId;
    private String message;
    private String date;
    private String type;
    private String readUnread;

    public NotificationClass(String objectId,String message, String date,String type,String readUnread) {
        this.objectId=objectId;
        this.message = message;
        this.date=date;
        this.type=type;
        this.readUnread=readUnread;
    }

    public String getReadUnread() {
        return readUnread;
    }

    public void setReadUnread(String readUnread) {
        this.readUnread = readUnread;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObjectIdd() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
