package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddEventPageAdmin extends Activity {

    ListView listview;
    List<ParseObject> ob;
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> adapter;
    ArrayList<Events> list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event_page_admin);
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Calender </font>"));
        new RemoteDataTask().execute();

    }

    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(AddEventPageAdmin.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Events from parse");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Locate the class table named "Country" in Parse.com
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                    "Events");
            query.orderByDescending("_created_at");
            try {
                ob = query.find();
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.eventList);
            // Pass the results into an ArrayAdapter
            adapter = new ArrayAdapter<String>(AddEventPageAdmin.this, R.layout.calander_rowlayout);
            // Retrieve object "name" from Parse.com database
            for (ParseObject events : ob) {
               // adapter.add((String) events.get("Title"));
                Date date=events.getDate("EventDate");
                Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String s = formatter.format(date);
                list.add(new Events((String)events.get("Title"),(String)events.get("Description"),s));


                // Binds the Adapter to the ListView
                listview.setAdapter(new Calendar_adapter(AddEventPageAdmin.this, list));

            }

            mProgressDialog.dismiss();
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    Intent i = new Intent(AddEventPageAdmin.this,
                            EventDetails.class);
                    i.putExtra("EventName", ob.get(position).getString("Title").toString());
                    Date date=ob.get(position).getDate("EventDate");
                    Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    String s = formatter.format(date);
                    i.putExtra("EventDate",s);
                    i.putExtra("EventDescription", ob.get(position).getString("Description").toString());
                    i.putExtra("EventLocation",ob.get(position).getString("EventLocation").toString());
                    startActivity(i);
                }
            });
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_add_event_page_admin,menu);
        return super.onCreateOptionsMenu(menu);


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.addEvent:
                Intent intent7 = new Intent(this,CalendarAdmin.class);
                startActivity(intent7);
                break;
            case R.id.item_adminhome:
                Intent intent3= new Intent(this, adminHomePage.class);
                startActivity(intent3);
                break;
            case R.id.item_studentList:
                Intent intent4= new Intent(this, StudentList_admin.class);
                startActivity(intent4);
                break;

            case R.id.item_adminevents:
                Intent intent = new Intent(this, AddEventPageAdmin.class);
                startActivity(intent);
                break;
            case R.id.item_messaging:
                Intent intent1 = new Intent(this, MessagingCentre.class);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
