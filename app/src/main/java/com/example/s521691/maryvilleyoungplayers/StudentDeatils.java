package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class StudentDeatils extends Activity {
    TextView firstName,lastName,stuID,foodAllergy,gender,grade,leadRole,otherSchool,pantsWaist,shirtSize,shoeSize,tshirtSize,picture,parentName,address,city,state,zip,extraShirtOne,extraShirtTwo,extraShirtThree,extraShirtFour,fee,paymentMethod,schoolName,phone,emailOne,emailTwo;
    ImageView childPicture;
    public static Intent childDetails;
    Bitmap bmp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_deatils);
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Student Details </font>"));
        firstName=(TextView) findViewById(R.id.firstname);
        lastName=(TextView) findViewById(R.id.lastname);
        stuID=(TextView) findViewById(R.id.studentID);
        foodAllergy=(TextView) findViewById(R.id.foodAllergy);
        gender=(TextView) findViewById(R.id.gender);
        grade=(TextView) findViewById(R.id.grade);
        leadRole=(TextView) findViewById(R.id.leadRole);
        otherSchool=(TextView) findViewById(R.id.otherSchool);
        pantsWaist=(TextView) findViewById(R.id.pantsWaist);
        shirtSize=(TextView) findViewById(R.id.shirtSize);
        shoeSize=(TextView) findViewById(R.id.shoeSize);
        tshirtSize=(TextView) findViewById(R.id.tshirtSize);
        parentName=(TextView) findViewById(R.id.parentName);
        address=(TextView) findViewById(R.id.address);
        city=(TextView) findViewById(R.id.city);
        state=(TextView) findViewById(R.id.state);
        zip=(TextView) findViewById(R.id.zip);
        extraShirtOne=(TextView) findViewById(R.id.extraShirtOne);
        extraShirtTwo=(TextView) findViewById(R.id.extraShirtTwo);
        extraShirtThree=(TextView) findViewById(R.id.extraShirtThree);
        extraShirtFour=(TextView) findViewById(R.id.extraShirtFour);
        fee=(TextView) findViewById(R.id.fee);
        paymentMethod=(TextView) findViewById(R.id.paymentMethod);
        schoolName=(TextView) findViewById(R.id.schoolName);
        phone=(TextView) findViewById(R.id.phone);
        emailOne=(TextView) findViewById(R.id.emailOne);
        emailTwo=(TextView) findViewById(R.id.emailTwo);


        childPicture=(ImageView) findViewById(R.id.childPicture);


        childDetails=getIntent();

        byte[] byteArray =childDetails.getByteArrayExtra("picture");
        bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        //eName=childDetails.getStringExtra("EventName");
        firstName.setText(childDetails.getStringExtra("FirstName"));
        //int ageIn=childDetails.getInExtra("Age");
        lastName.setText(childDetails.getStringExtra("LastName"));
        stuID.setText(childDetails.getStringExtra("StudentId"));
        foodAllergy.setText(childDetails.getStringExtra("FoodAllergy"));
        gender.setText(childDetails.getStringExtra("Gender"));
        grade.setText(childDetails.getStringExtra("Grade"));
        leadRole.setText(childDetails.getStringExtra("LeadRole"));
        otherSchool.setText(childDetails.getStringExtra("OtherSchool"));
        pantsWaist.setText(childDetails.getStringExtra("PantsWaist"));
        shirtSize.setText(childDetails.getStringExtra("shirtSize"));
        shoeSize.setText(childDetails.getStringExtra("shoeSize"));
        tshirtSize.setText(childDetails.getStringExtra("TshirtSize"));
        parentName.setText(childDetails.getStringExtra("ParentName"));
        address.setText(childDetails.getStringExtra("Address"));
        city.setText(childDetails.getStringExtra("city"));
        state.setText(childDetails.getStringExtra("state"));
        zip.setText(childDetails.getStringExtra("zip"));
        extraShirtOne.setText(childDetails.getStringExtra("extraShirtOne"));
        extraShirtTwo.setText(childDetails.getStringExtra("extraShirtTwo"));
        extraShirtThree.setText(childDetails.getStringExtra("extraShirtThree"));
        extraShirtFour.setText(childDetails.getStringExtra("extraShirtFour"));
        fee.setText(childDetails.getStringExtra("Fee"));
        paymentMethod.setText(childDetails.getStringExtra("PaymentMethod"));
        schoolName.setText(childDetails.getStringExtra("schoolName"));
        phone.setText(childDetails.getStringExtra("phone"));
        emailOne.setText(childDetails.getStringExtra("EmailOne"));
        emailTwo.setText(childDetails.getStringExtra("EmailTwo"));
//        childPicture.setImageBitmap(bmp);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_student_deatils, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.editChild:
                Intent intent8 = new Intent(StudentDeatils.this,EditStudentInfo.class);
                //intent8.putExtra("data",childDetails);
                startActivity(intent8);
                break;


        }
        return super.onOptionsItemSelected(item);
    }
}
