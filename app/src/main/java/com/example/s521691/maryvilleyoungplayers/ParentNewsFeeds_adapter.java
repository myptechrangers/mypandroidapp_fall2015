package com.example.s521691.maryvilleyoungplayers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by S521691 on 11/4/2015.
 */
public class ParentNewsFeeds_adapter extends BaseAdapter {
    ArrayList<ParentNewsFeeds> list;
    Context mContext;
    String type;

    public ParentNewsFeeds_adapter(Context con, ArrayList<ParentNewsFeeds> list) {
        this.mContext = con;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.parentnewsfeeds_layout, null);

            holder = new Holder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
         //   holder.desc = (TextView) convertView.findViewById(R.id.desc);
            holder.date=(TextView) convertView.findViewById(R.id.date);
            holder.image=(ImageView) convertView.findViewById(R.id.icon);


            convertView.setTag(holder);

        }
        else {

            holder = (Holder) convertView.getTag();
        }

        holder.title.setText(list.get(position).getDesc());
       // holder.desc.setText(list.get(position).getDesc());
        holder.date.setText(list.get(position).getDate());
        type=list.get(position).getType();
        if(type.equals("E")){
            holder.image.setImageResource(R.drawable.calendar);
        }
        else if(type.equals("N")){
            holder.image.setImageResource(R.drawable.notifications);
        }
        return convertView;
    }



    private class Holder {
        TextView title ;
        TextView desc;
        TextView date;
        ImageView image;


    }
}
