package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ChildInfo_Updated extends Activity {
    ListView listview;
    List<ParseObject> ob,obParent;
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> adapter;
    ArrayList<ChildrenData> list = new ArrayList<ChildrenData>();
    Bitmap bmp,bmp1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_info__updated);
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Children Information </font>"));
        new RemoteDataTask().execute();


    }
    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(ChildInfo_Updated.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Children from parse");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Locate the class table named "Country" in Parse.com
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Children");
            query.whereEqualTo("PhoneNumber",MainActivity.PH_NO);
            query.orderByDescending("_created_at");
            try {
                ob = query.find();

            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.childrenList);
            // Pass the results into an ArrayAdapter
            adapter = new ArrayAdapter<String>(ChildInfo_Updated.this,R.layout.childlist_rowlayout);
            // Retrieve object "name" from Parse.com database
            for (ParseObject children : ob) {

                final ChildrenData childData = new ChildrenData(children.get("FirstName").toString(), children.get("LastName").toString(), children.get("Cast").toString(), children.get("Role").toString(), " ", bmp);
                list.add(childData);


                ParseFile file=(ParseFile)children.getParseFile("Picture");


                try {
                    byte[] picFile = file.getData();
                    bmp = BitmapFactory.decodeByteArray(picFile,0,picFile.length);
                    childData.setChildImage(bmp);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


               adapter.add((String) children.get("FirstName"));



                ParseQuery<ParseObject> queryParent = new ParseQuery<ParseObject>("Parent");
                queryParent.whereEqualTo("PhoneNumber",children.getString("PhoneNumber"));
                try{
                    obParent=queryParent.find();
                }
                catch (ParseException e){
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }

            }

            listview.setAdapter(new ChildInfo_adapter(ChildInfo_Updated.this, list));
            // Binds the Adapter to the ListView
           // listview.setAdapter(adapter);

            // Close the progressdialog
            mProgressDialog.dismiss();
            // Capture button clicks on ListView items
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    ParseFile f=(ParseFile)ob.get(position).get("Picture");
                    try {
                        byte[] picFile = f.getData();
                        bmp1 = BitmapFactory.decodeByteArray(picFile,0,picFile.length);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp1.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    int x=obParent.size();

                    Intent i = new Intent(ChildInfo_Updated.this,
                            ChildrenDetails.class);


                    String studId=Integer.toString(ob.get(position).getInt("StudentID"));

                    i.putExtra("FirstName", ob.get(position).getString("FirstName").toString());
                    i.putExtra("LastName", ob.get(position).getString("LastName").toString());
                    i.putExtra("StudentId",studId);
                    i.putExtra("FoodAllergy", ob.get(position).getString("FoodAllergy").toString());
                    i.putExtra("Gender", ob.get(position).getString("Gender").toString());
                    i.putExtra("Grade", ob.get(position).getString("Grade").toString());
                    i.putExtra("LeadRole", ob.get(position).getString("LeadRole").toString());
                    i.putExtra("OtherSchool", ob.get(position).getString("OtherSchool").toString());
                    i.putExtra("PantsWaist", ob.get(position).getString("PantsWaist").toString());
                    i.putExtra("shirtSize", ob.get(position).getString("ShirtSize").toString());
                    i.putExtra("shoeSize", ob.get(position).getString("ShoeSize").toString());
                    i.putExtra("TshirtSize", ob.get(position).getString("TshirtSize").toString());
                    i.putExtra("Role",ob.get(position).getString("Role").toString());
                    i.putExtra("picture",byteArray);
                    i.putExtra("ParentName", obParent.get(x-1).getString("Name").toString());
                    i.putExtra("Address",obParent.get(x-1).getString("Address").toString());
                    i.putExtra("city",obParent.get(x-1).getString("City").toString());
                    i.putExtra("state",obParent.get(x-1).getString("State").toString());
                    i.putExtra("zip",obParent.get(x-1).getString("Zip").toString());
                    i.putExtra("extraShirtOne",obParent.get(x-1).getString("ExtraShirtOne").toString());
                    i.putExtra("extraShirtTwo",obParent.get(x-1).getString("ExtraShirtTwo").toString());
                    i.putExtra("extraShirtThree",obParent.get(x-1).getString("ExtraShirtThree").toString());
                    i.putExtra("extraShirtFour",obParent.get(x-1).getString("ExtraShirtFour").toString());
                    i.putExtra("fee",obParent.get(x-1).getString("Fee").toString());
                    i.putExtra("paymentMethod",obParent.get(x-1).getString("PaymentMethod").toString());
                    i.putExtra("schoolName",ob.get(x-1).getString("School").toString());
                    i.putExtra("phone",ob.get(x-1).getString("PhoneNumber"));
                    i.putExtra("EmailOne",obParent.get(x-1).getString("EmailOne").toString());
                    i.putExtra("EmailTwo",obParent.get(x-1).getString("EmailTwo").toString());

                    startActivity(i);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_child_info__updated, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_home:
                Intent intent7 = new Intent(this,parentHomeScreen.class);
                startActivity(intent7);
                break;
            case R.id.item_events:
                Intent intent = new Intent(this,CalanderParent_Updated.class);
                startActivity(intent);
                break;
            case R.id.item_childinfo:
                Intent intent1 = new Intent(this,ChildInfo_Updated.class);
                startActivity(intent1);
                break;
            case R.id.item_contactus:
                Intent intent2 = new Intent(this,contactUs.class);
                startActivity(intent2);
                break;
            case R.id.item_notifications:
                Intent intent3 = new Intent(this,Notifications.class);
                startActivity(intent3);
                break;
            case R.id.item_donateus:
                Intent intent4 = new Intent(this,ParentDonation.class);
                startActivity(intent4);
                break;
            case R.id.item_socialmedia:
                Intent intent5 = new Intent(this,SocialMedia.class);
                startActivity(intent5);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
