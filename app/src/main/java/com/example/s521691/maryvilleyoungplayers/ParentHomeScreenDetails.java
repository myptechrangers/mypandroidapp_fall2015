package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ParentHomeScreenDetails extends Activity {
    TextView title,description,date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_home_screen_details);
        title=(TextView) findViewById(R.id.Title);
        description=(TextView) findViewById(R.id.Description);
        date=(TextView) findViewById(R.id.Date);

        Intent i=getIntent();

        title.setText(i.getStringExtra("Title"));
        description.setText(i.getStringExtra("Desc"));
        date.setText(i.getStringExtra("date"));

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > News Feed Details </font>"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_parent_home_screen_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
