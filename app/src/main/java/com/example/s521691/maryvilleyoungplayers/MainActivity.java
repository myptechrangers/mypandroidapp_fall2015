package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    static String PH_NO;
    Button button;
    EditText myEdit;
    Intent intent;
    List<ParseObject> ob,obParent;
    ArrayList<String> phoneNumbersParent=new ArrayList<>();
    ArrayList<String> phoneNumbersAdmin=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "te3vB0VSMYnNNwOOh3sqcM2WJuYcyhl2iACXdizv", "bMMDnKwlhmVBjPmP6cqKB48EaziLFNpWMSa9Uzps");


        new RemoteDataTask().execute();
        addListenerOnButton();

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Maryville Young Players </font>"));
    }


    public void addListenerOnButton() {

        final Context context = this;

        button = (Button) findViewById(R.id.loginBTN);
        myEdit = (EditText)findViewById(R.id.numTxt);


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (phoneNumbersAdmin.contains(myEdit.getText().toString())) {
                    PH_NO=myEdit.getText().toString();
                    intent = new Intent(context, adminHomePage.class);
                    startActivity(intent);
                } else if (phoneNumbersParent.contains(myEdit.getText().toString())) {
                    PH_NO=myEdit.getText().toString();
                    Intent intent1 = new Intent(context, parentHomeScreen.class);
                    startActivity(intent1);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Invalid Phone Number!!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Notifications.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("Events from parse");
//            // Set progressdialog message
//            mProgressDialog.setMessage("Loading...");
//            mProgressDialog.setIndeterminate(false);
//            // Show progressdialog
//            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Locate the class table named "Country" in Parse.com
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("AdminContacts");
            query.orderByDescending("_created_at");
            ParseQuery<ParseObject> queryParents = new ParseQuery<ParseObject>("Parent");
            try {
                ob = query.find();
                obParent=queryParents.find();
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            for (ParseObject adminPhNo : ob) {
                String x=(String)adminPhNo.get("PhoneNumber");
                phoneNumbersAdmin.add(x);
            }
            for(ParseObject parentPhNo:obParent){
                phoneNumbersParent.add((String)parentPhNo.get("PhoneNumber"));
            }

        }
    }


}
