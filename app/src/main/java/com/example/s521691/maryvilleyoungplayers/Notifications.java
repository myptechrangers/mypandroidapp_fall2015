package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Notifications extends Activity {

    ListView listview;
    List<ParseObject> ob;
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> adapter;
    ArrayList<NotificationClass> list=new ArrayList<>();
    String Objid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        new RemoteDataTask().execute();

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Notifications </font>"));
    }

    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Notifications.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Notifications from parse");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Notifications");
            query.whereEqualTo("PhoneNumber",MainActivity.PH_NO);
            query.orderByDescending("_created_at");
            try {
                ob = query.find();

            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            listview = (ListView) findViewById(R.id.notificationsList);
            adapter = new ArrayAdapter<String>(Notifications.this,R.layout.notifications_rowlayout);
            for (ParseObject messages : ob) {
                adapter.add((String) messages.get("Message"));
                Date date=messages.getCreatedAt();
                Format formatter = new SimpleDateFormat("MMM dd, yyyy HH:mm a");
                String s = formatter.format(date);
                try {
                    messages.save();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                list.add(new NotificationClass( messages.getObjectId(),(messages.get("Message")).toString(),s,(messages.get("MessageType")).toString(),messages.get("IsRead").toString()));
            }
            listview.setAdapter(new Notification_adapter(Notifications.this, list));
            mProgressDialog.dismiss();
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    ParseQuery<ParseObject> query=ParseQuery.getQuery("Notifications");
                    String idd=list.get(position).getObjectIdd();
                    query.getInBackground(idd, new GetCallback<ParseObject>() {
                                public void done(ParseObject msg, ParseException e) {
                                    if (e == null) {

                                        msg.put("IsRead", "R");
                                        msg.saveInBackground();
                                    }
                                }
                            });
                    Intent refresh=new Intent(Notifications.this,Notifications.class);
                    startActivity(refresh);
                    finish();
                    Intent i = new Intent(Notifications.this, NotificationDetails.class);
                    // Pass data "name" followed by the position
                    i.putExtra("Message", ob.get(position).getString("Message").toString());
                    Date date=ob.get(position).getCreatedAt();
                    Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    String s = formatter.format(date);
                    i.putExtra("MessageDate",s);
                    i.putExtra("MessageType",ob.get(position).getString("MessageType"));

                    startActivity(i);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_notifications,menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_home:
                Intent intent7 = new Intent(this,parentHomeScreen.class);
                startActivity(intent7);
                break;
            case R.id.item_events:
                Intent intent = new Intent(this,CalanderParent_Updated.class);
                startActivity(intent);
                break;
            case R.id.item_childinfo:
                Intent intent1 = new Intent(this,ChildInfo_Updated.class);
                startActivity(intent1);
                break;
            case R.id.item_contactus:
                Intent intent2 = new Intent(this,contactUs.class);
                startActivity(intent2);
                break;
            case R.id.item_notifications:
                Intent intent3 = new Intent(this,Notifications.class);
                startActivity(intent3);
                break;
            case R.id.item_donateus:
                Intent intent4 = new Intent(this,ParentDonation.class);
                startActivity(intent4);
                break;
            case R.id.item_socialmedia:
                Intent intent5 = new Intent(this,SocialMedia.class);
                startActivity(intent5);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
