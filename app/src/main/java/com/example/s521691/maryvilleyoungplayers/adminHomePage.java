package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class adminHomePage extends Activity {
    ListView listview;
    List<ParseObject> ob,obEvents;
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> adapter;
    ArrayList<ParentNewsFeeds> list=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home_page);
        new RemoteDataTask().execute();
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Home </font>"));

    }

    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(adminHomePage.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Data from parse");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Notifications");
            query.whereEqualTo("IsAdmin","Y");
            query.orderByAscending("_created_at");
            ParseQuery<ParseObject> queryEvents = new ParseQuery<ParseObject>("Events");
            queryEvents.orderByAscending("_created_at");
            try {
                ob = query.find();
                obEvents=queryEvents.find();
//                for(int i=0;i<ob.size();i++){
//                    obAll.add(ob.get(i));
//                }
//                for(int i=0;i<obEvents.size();i++){
//                    obAll.add(obEvents.get(i));
//                }
//                ConfigurationBuilder cb=new ConfigurationBuilder();
//                cb.setOAuthConsumerKey("RCHsSoVYINgaFJ0T9klbYqmUv\n");
//                cb.setOAuthConsumerSecret("kBjrMdji5zfOHRE3z9tWpEvO8vBQTAwRL4izmtAcizQLD9dgKj");
//                cb.setOAuthAccessToken("3828820872-ciHRYtDidPHcDwfzdTQY2foJ3nXq7ki1gvBgfgC");
//                cb.setOAuthAccessTokenSecret("rSCPUvM9Sk7G8Bk1hwiW6YuRjDsrZ4mftVeudHcmNt4QH");
//                TwitterFactory tf=new TwitterFactory(cb.build());
//                Twitter twitter=tf.getInstance();
//                Paging paging=new Paging(1,5);
//                List<Status> statuses=twitter.getUserTimeline();
//              //  statuses=twitter.getUserTimeline();
////                list.clear();
//                for (    Status s : statuses) {
//                    list.add(new ParentNewsFeeds("",s.toString()," "," "," "));
//                }
//                //mTweets.notifyDataSetChanged();
//            }
//            catch (  TwitterException e) {
//                e.printStackTrace();
//            }


            }

            catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            catch(Exception ex){
                Log.e("TwitterFeedActivity", "Error loading JSON", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.newsFeedsAdmin);
            // Pass the results into an ArrayAdapter
            adapter = new ArrayAdapter<String>(adminHomePage.this,R.layout.parentnewsfeeds_layout);
            for (ParseObject messages : ob) {
                adapter.add((String) messages.get("Message"));
                Date date=messages.getCreatedAt();
                Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
                String s = formatter.format(date);
                list.add(new ParentNewsFeeds("Message", (messages.get("Message")).toString(),s,"N",""));
            }
            for(ParseObject events:obEvents){
                Date date=events.getDate("EventDate");
                Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
                String s = formatter.format(date);
                list.add(new ParentNewsFeeds((String)events.get("Title"),events.get("Description").toString(),s,"E",""));
            }
            Collections.sort(list, new Comparator<ParentNewsFeeds>() {
                @Override
                public int compare(ParentNewsFeeds lhs, ParentNewsFeeds rhs) {
                    return rhs.getDate().compareTo(lhs.getDate());
                }
            });
            listview.setAdapter(new ParentNewsFeeds_adapter(adminHomePage.this, list));

            // Close the progressdialog
            mProgressDialog.dismiss();
            // Capture button clicks on ListView items
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent i = new Intent(adminHomePage.this, ParentHomeScreenDetails.class);
                    // Pass data "name" followed by the position
                    i.putExtra("Title", list.get(position).getTitle());
                    i.putExtra("Desc",list.get(position).getDesc());
                    i.putExtra("date",list.get(position).getDate());

                    startActivity(i);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_admin_login_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.item_adminhome:
                Intent intent3= new Intent(this, adminHomePage.class);
                startActivity(intent3);
                break;
            case R.id.item_studentList:
                Intent intent4= new Intent(this, StudentList_admin.class);
                startActivity(intent4);
                break;

            case R.id.item_adminevents:
                Intent intent = new Intent(this, AddEventPageAdmin.class);
                startActivity(intent);
                break;
            case R.id.item_messaging:
                Intent intent1 = new Intent(this, MessagingCentre.class);
                startActivity(intent1);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
