package com.example.s521691.maryvilleyoungplayers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.parse.ParseObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

//import android.app.DatePickerDialog;
//import android.app.Dialog;
//import android.widget.DatePicker;
//import android.widget.ImageButton;

public class CalendarAdmin extends Activity {

    EditText name,date,desc;
    private Spinner location;
    Button add;

    DateFormat fmtDateAndTime;
    TextView lblDateAndTime;
    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            myCalendar.set(Calendar.MINUTE, minute);
            updateLabel();
        }
    };

    public CalendarAdmin() {
        fmtDateAndTime = DateFormat.getDateTimeInstance();
    }

    private void updateLabel() {
        lblDateAndTime.setText(fmtDateAndTime.format(myCalendar.getTime()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_admin);
         getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#024700")));
        bar.setTitle(Html.fromHtml("<font color='#FFFFFF' > Add Event </font>"));


        name=(EditText) findViewById(R.id.name);
        date=(EditText) findViewById(R.id.date);
        desc=(EditText) findViewById(R.id.description);
//        location=(EditText) findViewById(R.id.location);
        add=(Button) findViewById(R.id.addCalanderAdminPage);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseObject events = new ParseObject("Events");
                events.put("Title", name.getText().toString());
                SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy, HH:mm a");
                // Date d=format.parse(lblDateAndTime.getText().toString());
                    fmtDateAndTime.format(myCalendar.getTime());
//                    Date d=format.parse(fmtDateAndTime.format(myCalendar.getTime()));
                events.put("EventDate", fmtDateAndTime.format(myCalendar.getTime()));
                events.put("Description", desc.getText().toString());
                events.saveInBackground();

                Intent i = new Intent(getApplicationContext(), AddEventPageAdmin.class);
                startActivity(i);
            }
        });

        lblDateAndTime = (TextView) findViewById(R.id.date);
        Button btnDate = (Button) findViewById(R.id.btnDate);
        btnDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new DatePickerDialog(CalendarAdmin.this, d, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Button btnTime = (Button) findViewById(R.id.btnTime);
        btnTime.setOnClickListener(new View.OnClickListener() {
            public  void onClick(View v) {
                new TimePickerDialog(CalendarAdmin.this, t, myCalendar
                        .get(Calendar.HOUR_OF_DAY), myCalendar
                        .get(Calendar.MINUTE), true).show();
            }
        });

        updateLabel();
    }


    public void addListenerOnSpinnerItemSelection(){

        location = (Spinner) findViewById(R.id.location);
        location.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    public void addListenerOnButton() {

        location = (Spinner) findViewById(R.id.location);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calendar_admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.item_adminhome:
                Intent intent3= new Intent(this, adminHomePage.class);
                startActivity(intent3);
                break;
            case R.id.item_studentList:
                Intent intent4= new Intent(this, StudentList_admin.class);
                startActivity(intent4);
                break;

            case R.id.item_adminevents:
                Intent intent = new Intent(this, AddEventPageAdmin.class);
                startActivity(intent);
                break;
            case R.id.item_messaging:
                Intent intent1 = new Intent(this, MessagingCentre.class);
                startActivity(intent1);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
